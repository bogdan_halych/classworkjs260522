import React, { useState, useEffect } from "react";
import { Table } from "./components/table";

function App() {
  let [curs, setCurs] = useState([]);
  const day = new Date().getDate();
  const now = new Date().getFullYear() + "-" + new Date().getMonth() + "-" + new Date().getDate();
  useEffect(() => {
    fetch(
      "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    ).then((res) => res.json()).then((res) => setCurs(res));
  }, [day]);
  return (
    <>
      <h2>Course: {now}</h2>
      <Table data={curs} />
    </>
  );
}
export default App;