import React from "react";

export function Table(props) {
  return (
    <table>
      <tbody>
        <tr className="first">
          <td>Name</td>
          <td>Rate</td>
          <td>CC</td>
        </tr>
        {props.data.map((el, index) => (
          <tr key={index}>
            <td>{el.txt}</td>
            <td>{el.rate}</td>
            <td>{el.cc}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
