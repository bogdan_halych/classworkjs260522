//const myArray = new Array(); //або
const myArray = [];
for (let i = 0; i < 15; i++) {
  myArray[i] = (i + 1) + " -" + Math.floor(Math.random() * 10000);
}

document.write("My array before method reverse() - " + myArray + ".<br><br>");
myArray.reverse();
document.write("My array after method reverse() = " + myArray.join(", &#9729;") + "<br><hr>");

let indexForDelete = prompt("Input index from 0 to 14");
if (confirm("Do you want delete element with index " + indexForDelete + "?")) {
  let delElement = myArray.splice(indexForDelete, 1);
  alert("You delete " + delElement);
}

document.write("My array after deleting element with index - " + indexForDelete + " - " + myArray.join(", &#9729;"));