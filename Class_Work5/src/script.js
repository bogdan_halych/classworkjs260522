// Создайте объект криптокошилек.В кошельке должно хранится имя владельца,
//   несколько валют Bitcoin, Ethereum, Stellar и в каждой валюте дополнительно
//    есть имя валюты, логотип, несколько монет и курс на сегодняшний день.
//    Также в объекте кошелек есть метод при вызове которого он принимает имя валюты
//     и выводит на страницу информацию.
//      "Добрый день, ... ! На вашем балансе (Название валюты и логотип)
//       осталось N монет, если вы сегодня продадите их то, получите ...грн.
//       Вывод на страницу должен быть касиво формлен с использованием css и html.

const wallet = {
  name: "Bogdan",
  btc: {
    name: "Bitcoin",
    logo: "<img src ='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png'>",
    rate: "30245.99",
    coin: "54"
  },
  eth: {
    name: "Ethereum",
    logo: "<img src ='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png'>",
    rate: "1803.16",
    coin: "27"
  },
  ste: {
    name: "Stellar",
    logo: "<img src ='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png'>",
    rate: "0140.3",
    coin: "37"
  },
  show: function (nameCoin) {
    document.getElementById("wallet").innerHTML = `Добрий день ${wallet.name}! На вашому балансі 
    ${wallet[nameCoin].name} ${wallet[nameCoin].logo} залишилось ${wallet[nameCoin].coin} монет. Якщо ви сьогодні продасте, то отримаєте 
    ${(wallet[nameCoin].rate*wallet[nameCoin].coin*31).toFixed(2)}грн`
  }
}


wallet.show(prompt("Input coin", "btc, eth or ste"));