


//1. Якщо змінна a дорівнює 10, то виведіть 'Вірно', інакше виведіть 'Неправильно'.

let numInputed = parseInt(prompt("Вгадайте число", "5"));
let res = numInputed === 10 ? "Вірно!" : "Неправильно!";
alert(res);
//________________________________________________________________________

// 2. У змінній min лежить число від 0 до 59. Визначте, в яку чверть години потрапляє це число
//   (У першу, другу, третю або четверту).

let numberInputed = parseInt(prompt("Вгадайте число", "5"));

if (numberInputed >= 0 && numberInputed < 15) {
  alert("Число у 1-ій чверті");
} else if (numberInputed < 30) {
  alert("Число у 2-ій чверті");
} else if (numberInputed < 45) {
  alert("Число у 3-ій чверті");
} else if (numberInputed <= 59) {
  alert("Число у 4-ій чверті");
} else {
  alert("Невірно введені дані. Введіть число у проміжку від 0 до 59");
}; 

// ______________________________________________________________________________

//3 .Змінна num може приймати 4 значення: 1, 2, 3 або 4. Якщо вона має значення '1', то змінну result запишемо 'зима', якщо має значення
//'2' – 'весна' тощо.Розв'яжіть завдання через switch-case.

let numSeason = prompt("Введіть номер пори року", "2");

switch (numSeason) {
  case "1": {
    alert("Winter");
    break;
  }
  case "2": {
    alert("Spring");
    break;
  }
  case "3": {
    alert("Summer");
    break;
  }
  case "4": {
    alert("Autumn");
    break;
  }
  default: {
    alert("Введіть правильні дані (1,2,3 або 4)")
    }
}

// ____________________________________________________________________________________

//4. Використовуючи цикли та умовні конструкції намалюйте ялинку

document.write('<div style="text-align: center; line-height: 0.5">')
for (let i = 0; i < 5; i++) {
  for (j = i; j >= 0; j--) {
    document.write(" * ");
  }
  document.write("<br>");
}
for (let i = 0; i < 10; i++) {
  for (j = i; j >= 0; j--) {
    document.write(" * ");
  }
  document.write("<br>");
}
for (let i = 0; i < 15; i++) {
  for (j = i; j >= 0; j--) {
    document.write(" * ");
  }
  document.write("<br>");
}

// ______________________________________________________________________________________

// 5. Використовуючи умовні конструкції перевірте вік користувача,
//   якщо користувачеві буде від 18 до 35 років переведіть його на сайт google.com 
//   через 2 секунди, якщо вік користувача буде від 35 до 60 років,
//   переведіть його на сайт https://www.uz.gov.ua/, якщо користувачеві буде до 18 років покажіть йому першу серію лунтика з ютубу
// Виконайте це завдання за допомогою if, switch, тернарний оператор


let age = prompt('Input your age');
if (age >=18 && age < 35) {
  document.write('<meta http-equiv="refresh" content="2, url=http://google.com">');
}
else if (age >= 35) {
  document.write('<meta http-equiv="refresh" content="2, url=http://www.uz.gov.ua">');
}
else if (age <= 18 && age >= 0) {
  document.write('<meta http-equiv="refresh" content="2, url=https://www.youtube.com/watch?v=3Hz3kHRRISU">');
}
else {
  document.write('Incorrect data');
};